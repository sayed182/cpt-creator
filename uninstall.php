<?php
// Silence is golden


$plugin_slug = "post_type_creator";
$plugin_options = 'custom_post_creator_details';
if( !get_option($plugin_options, true)){
    echo "<script>alert('There was some error deleting the plugin.')</script>";
    return;
}
$PostData = get_option($plugin_options, true);
$data = @unserialize($PostData);
unregister_post_type($data['slug']);
delete_option($plugin_options);
flush_rewrite_rules(true);

global $wpdb;
$result1 = $wpdb->delete('wp_posts', ['post_type' => $data['slug']]);
$result2 = $wpdb->delete('wp_postmeta', ['meta_key' => $data['slug'].'_cpt_value']);


