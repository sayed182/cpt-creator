<?php

/**
 * Fired during plugin activation
 *
 * @link       https://wordpress.org/
 * @since      1.0.0
 *
 * @package    Web_Portfolio
 * @subpackage Web_Portfolio/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Web_Portfolio
 * @subpackage Web_Portfolio/includes
 * @author     WordPress Dev <admin@wordpress.org>
 */
class Web_Portfolio_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {
		update_option(PLUGIN_SLUG, true);
	}

}
