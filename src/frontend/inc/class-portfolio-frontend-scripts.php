<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       https://wordpress.org/
 * @since      1.0.0
 *
 * @package    Web_Portfolio
 * @subpackage Web_Portfolio/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Web_Portfolio
 * @subpackage Web_Portfolio/admin
 * @author     WordPress Dev <admin@wordpress.org>
 */
class Plugin_Load_Frontend_Scripts
{

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct($plugin_name, $version)
	{

		$this->plugin_name = $plugin_name;
		$this->version = $version;
	}

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles()
	{

		/**
		 *
		 */

		// wp_enqueue_style( $this->plugin_name, plugin_dir_url( dirname(__FILE__) ) . 'css/bootstrap.min.css', array(), $this->version, 'all' );
		wp_enqueue_style($this->plugin_name, plugin_dir_url(dirname(__FILE__)) . 'css/style.css', null, '1.0');
	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts()
	{

		// wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/main.js', array( 'jquery' ), $this->version, true );
		// wp_localize_script('sa-cpt-main-js', 'WPURLS', array('siteurl' => get_option('siteurl')));
		wp_enqueue_script($this->plugin_name, plugin_dir_url(dirname(__FILE__)) . 'js/script.js', array('jquery'), '1.0', true);
		wp_localize_script($this->plugin_name, 'WPURLS', array(
			'ajaxurl' => admin_url('admin-ajax.php'),
			'siteurl' => get_option('siteurl')
		));
	}
	// ----------- PAGINATION ---------------//
	function get_posts_for_pagination()
	{
		$html = 'Hello World';
		$paged = ($_GET['page']) ? $_GET['page'] : 1;
		$post_type = $_GET['posttype'];

		if (empty($post_type)) {
			echo 'Hello Posst t';
		}

		if (filter_var(intval($paged), FILTER_VALIDATE_INT)) {

			$args = array(
				'post_type' => $post_type,
				'paged' => $paged,
				'posts_per_page' => 4,
				'post_status' => 'publish'
			);

			$loop = new WP_Query($args);

			$post_count = $loop->found_posts;
			$max_num_pages = $loop->max_num_pages;

			if ($loop->have_posts()) {
				while ($loop->have_posts()) {
					$loop->the_post();
					$post_link = get_permalink();
?>
					<div class="col mt-4">

						<div class="card h-100 shadow border-0 " style="background-color: #ecf6fe;">
							<a class="nav-link p-0" href="<?php echo esc_url($post_link) ?>">
								<img src="<?php echo get_the_post_thumbnail_url(get_the_ID(), 'full'); ?>" style="height:20rem;" class="card-img-top" alt="...">
							</a>
							<div class="card-body">
								<a class="nav-link p-0" href="<?php echo esc_url($post_link) ?>">
									<h5 class="card-title text-body"><?php echo esc_html(get_the_title()) ?></h5>
								</a>
							</div>
							<div class="card-footer d-flex align-items-center justify-content-between p-4 my-4 bg-transparent">
								<div>
									<a href="<?php echo esc_url($post_link) . '#comments' ?>" class="text-reset text-decoration-none d-flex align-items-center justify-content-center">
										<span class="h-svg-icon">
											<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" id="comments" viewBox="0 0 512 545.5" height="30">
												<path d="M32 112h320v256H197.5L122 428.5l-26 21V368H32V112zm32 32v192h64v46.5l54-43 4.5-3.5H320V144H64zm320 32h96v256h-64v81.5L314.5 432h-149l40-32h120l58.5 46.5V400h64V208h-64v-32z"></path>
											</svg>
										</span>
										&nbsp; <?php echo get_comments_number(); ?>
									</a>
								</div>
								<div>
									<a href="<?php echo esc_url($post_link) ?>" class="text-reset text-decoration-none">
										read more
										<span class="h-svg-icon h-button__icon style-123-icon style-local-147-m17-icon">
											<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" id="arrow-right" viewBox="0 0 512 545.5" height="25">
												<path d="M299.5 140.5l136 136 11 11.5-11 11.5-136 136-23-23L385 304H64v-32h321L276.5 163.5z"></path>
											</svg>
										</span>
									</a>

								</div>
							</div>
						</div>
					</div>
<?php

				}
				wp_reset_query();
			}
		}

		wp_die();
	}
}
