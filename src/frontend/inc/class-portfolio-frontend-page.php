<?php

class Plugin_Frontend_pages
{

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct()
	{
		add_filter('single_template', array($this, 'get_single_template'));
		add_filter('archive_template', array($this, 'get_archive_template'));
	}

	public function get_single_template($single_template)
	{
		global $post;

		$post_name = get_option(POST_TYPE_DETAIL, true);
		$data = @unserialize($post_name);
		if ($post != null && $post->post_type == $data['slug']) {
			$single_template = plugin_dir_path(dirname(__FILE__)) . '/template-parts/single.php';
		}
		return $single_template;
	}

	public function get_archive_template($archive_template){
		global $post;

		$post_name = get_option(POST_TYPE_DETAIL, true);
		$data = @unserialize($post_name);
		if ($post != null && $post->post_type == $data['slug']) {
			$archive_template = plugin_dir_path(dirname(__FILE__)) . '/template-parts/archive.php';
		}
		return $archive_template;
	}

}
new Plugin_Frontend_pages();
