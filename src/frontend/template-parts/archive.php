<?php
if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly.
}
get_header();
?>

<main class="site-main container" role="main">

    <div class="row">
        <div class="col-md-9">
            <header class="page-header">
                <?php
                // the_archive_title('<h1 class="entry-title">', '</h1>');
                // the_archive_description('<p class="archive-description">', '</p>');
                ?>
            </header>
            <div class="page-content mt-5">
                <div class="row row-cols-2 row-cols-md-2 m-0" id="post-container">
                    <?php
                    while (have_posts()) {
                        the_post();
                        $post_link = get_permalink();
                    ?>

                        <div class="col mt-4">

                            <div class="card h-100 shadow border-0 " style="background-color: #ecf6fe;">
                                <a class="nav-link p-0" href="<?php echo esc_url($post_link) ?>">
                                    <div style="height: 15rem; overflow: hidden;" class="d-flex align-items-center justify-content-center">
                                    <?php if(get_the_post_thumbnail_url(get_the_ID(), 'full')){ ?>
                                        <img src="<?php echo get_the_post_thumbnail_url(get_the_ID(), 'full'); ?>" class="card-img-top w-100" alt="...">
                                    <?php }else{?>
                                        <img src="https://via.placeholder.com/728x300?text=<?php echo get_the_title();?>" class="card-img-top w-100 h-100" alt="...">
                                    <?php } ?>
                                    </div>

                                </a>
                                <div class="card-body">
                                    <p>by <span class="text-info"><?php echo get_the_author(); ?></span><br><span class="text-info"><?php echo get_the_date(); ?></span></p>
                                    <a class="nav-link p-0" href="<?php echo esc_url($post_link) ?>">
                                        <h5 class="card-title text-body"><?php echo ucwords(esc_html(get_the_title())) ?></h5>
                                    </a>
                                    <p class="card-text text-body"><?php  ?></p>
                                </div>
                                <div class="card-footer d-flex align-items-center justify-content-between p-4 my-4 bg-transparent">
                                    <div>
                                        <a href="<?php echo esc_url($post_link).'#comments' ?>" class="text-reset text-decoration-none d-flex align-items-center justify-content-center">
                                            <span class="h-svg-icon">
                                                <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" id="comments" viewBox="0 0 512 545.5" height="30">
                                                    <path d="M32 112h320v256H197.5L122 428.5l-26 21V368H32V112zm32 32v192h64v46.5l54-43 4.5-3.5H320V144H64zm320 32h96v256h-64v81.5L314.5 432h-149l40-32h120l58.5 46.5V400h64V208h-64v-32z"></path>
                                                </svg>
                                            </span>
                                            &nbsp; <?php echo get_comments_number(); ?>
                                        </a>
                                    </div>
                                    <div>
                                        <a href="<?php echo esc_url($post_link) ?>" class="text-reset text-decoration-none">
                                            read more
                                            <span class="h-svg-icon h-button__icon style-123-icon style-local-147-m17-icon">
                                                <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" id="arrow-right" viewBox="0 0 512 545.5" height="25">
                                                    <path d="M299.5 140.5l136 136 11 11.5-11 11.5-136 136-23-23L385 304H64v-32h321L276.5 163.5z"></path>
                                                </svg>
                                            </span>
                                        </a>

                                    </div>
                                </div>
                            </div>
                        </div>


                    <?php } ?>

                </div>

            </div>

            <!-- Pagination -->
            <div class="border-top mt-4">
            <?php
            global $wp_query;
            if ($wp_query->max_num_pages > 1) :
            ?>
                <?php if (get_option('sa_cpt_pagination_type') == 'number') { ?>
                    <div class="d-flex w-100 justify-content-end mt-4">
                        <?php posts_nav(); ?>
                    </div>
                <?php } else { ?>
                    <div id="pagination" class="d-block text-center mt-4">
                        <a href="page/2/" class="infinite-more next"><span class="sprite"></span>More</a>
                        <span id="pagination-post-type" class="d-none"><?php echo get_option('sa_cpt_slug');?></span>
                    </div>
                <?php } ?>

            <?php endif; ?>
            </div>
            

        </div>
        <div class="col-md-3">
            <?php get_sidebar(); ?>
        </div>
    </div>


    <?php wp_link_pages(); ?>

</main>

<?php

get_footer();

function posts_nav()
{

    if (is_singular())
        return;

    global $wp_query;

    /** Stop execution if there's only 1 page */
    if ($wp_query->max_num_pages <= 1)
        return;

    $paged = get_query_var('paged') ? absint(get_query_var('paged')) : 1;
    $max   = intval($wp_query->max_num_pages);

    /** Add current page to the array */
    if ($paged >= 1)
        $links[] = $paged;

    /** Add the pages around the current page to the array */
    if ($paged >= 3) {
        $links[] = $paged - 1;
        $links[] = $paged - 2;
    }

    if (($paged + 2) <= $max) {
        $links[] = $paged + 2;
        $links[] = $paged + 1;
    }

    echo '<nav aria-label="Page navigation example"><ul class="pagination">' . "\n";

    /** Previous Post Link */
    if (get_previous_posts_link())
        printf('<li>%s</li>' . "\n", get_previous_posts_link("Previous"));

    /** Link to first page, plus ellipses if necessary */
    if (!in_array(1, $links)) {
        $class = 1 == $paged ? ' class="active page-item"' : '';

        printf('<li%s><a href="%s" class="">%s</a></li>' . "\n", $class, esc_url(get_pagenum_link(1)), '1');

        if (!in_array(2, $links))
            echo '<li>…</li>';
    }

    /** Link to current page, plus 2 pages in either direction if necessary */
    sort($links);
    foreach ((array) $links as $link) {
        $class = $paged == $link ? ' class="active "' : ' "';
        printf('<li%s><a href="%s" class="">%s</a></li>' . "\n", $class, esc_url(get_pagenum_link($link)), $link);
    }

    /** Link to last page, plus ellipses if necessary */
    if (!in_array($max, $links)) {
        if (!in_array($max - 1, $links))
            echo '<li>…</li>' . "\n";

        $class = $paged == $max ? ' class="active page-item"' : ' ';
        printf('<li%s><a href="%s" class="">%s</a></li>' . "\n", $class, esc_url(get_pagenum_link($max)), $max);
    }

    /** Next Post Link */
    if (get_next_posts_link())
        printf('<li class="">%s</li>' . "\n", get_next_posts_link("Next"));

    echo '</ul></nav>' . "\n";
}
