<?php
if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly.
}

get_header();
?>
<?php

while (have_posts()) :
    the_post();
    $custom_fields = get_post_meta($post->ID, $post->post_type . '_cpt_value', true);
?>

    <main <?php post_class('site-main container mt-5'); ?> role="main">
        <?php if (apply_filters('hello_elementor_page_title', true)) : ?>

        <?php endif; ?>
        <div class="row">
            <div class="col-md-9">
                <div class="page-content card shadow-lg border-0">
                    <?php if (get_the_post_thumbnail_url(get_the_ID(), 'full')) { ?>
                        <img src="<?php echo get_the_post_thumbnail_url(get_the_ID(), 'full');  ?>" class="card-img-top d-block mx-auto" alt="..." style="height:30rem;">
                    <?php } else { ?>
                        <img src="https://via.placeholder.com/728x500?text=<?php echo get_the_title(); ?>" class="card-img-top d-block mx-auto" alt="..." style="height:30rem;">
                    <?php } ?>


                    <div class="card-body">
                        <p>by <span class="text-info"><?php echo get_the_author(); ?></span> on <span class="text-info"><?php echo get_the_date(); ?></span></p>
                        <?php the_content(); ?>
                        <?php $keys = get_post_custom_keys();
                        if ($keys) {
                            $li_html = '';
                            foreach ((array) $custom_fields as $key => $value) {
                                $keyt = trim($key);
                                if (is_protected_meta($keyt, 'post')) {
                                    continue;
                                }
                                $html = sprintf(
                                    "<li class='list-group-item'>
                    <div class='row'>
                    <div class='col-md-3 col-sm-6'>
                    %s
                    </div>
                    <div class='col-md-9 col-sm-6'>
                    %s
                    </div>
                    </div>
                    </li>\n",
                                    sprintf(_x('%s', 'Post custom field name'), $key),
                                    $value
                                );
                                $li_html .= apply_filters('the_meta_key', $html, $key, $value);
                            }
                            if ($li_html) {
                                echo "<ul class='list-group list-group-flush'>\n{$li_html}</ul>\n";
                            }
                        }
                        ?>


                        <?php

                        echo '<ul class="list-unstyled mx-0  py-3 ps-3">';
                        // echo wpdocs_custom_taxonomies_terms_links();
                        echo '</ul>';
                        ?>


                        <div class="border-top border-bottom py-3 px-2 bg-transparent d-flex justify-content-between post-navigation">
                            <?php
                            echo previous_post_link('%link', '<span class="text-secondary text-decoration-none">Previous Post</span>'); ?>
                            <?php
                            echo next_post_link('%link', '<span class="text-secondary text-decoration-none">Next Post</span>'); ?>
                        </div>
                        <div class=" mt-4">
                            <?php
                            if (comments_open() || get_comments_number()) {
                                comments_template();
                            } else {
                                echo "<p class='text-center'>Comments are disabled</p>";
                            }

                            ?>
                        </div>
                    </div>

                </div>
            </div>
            <div class="col-md-3">
                <?php get_sidebar(); ?>
            </div>
        </div>

    </main>


<?php
endwhile;
get_footer();
