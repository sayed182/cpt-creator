<?php

class Plugin {


    public function __construct(){
		$this->load_dependencies();
		$this->define_admin_hooks();
		$this->define_public_hooks();
    }

    private function load_dependencies() {

		/**
		 * The class responsible for orchestrating the actions and filters of the
		 * core plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'src/admin/inc/class-portfolio-admin-scripts.php';
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'src/admin/inc/class-portfolio-admin-pages.php';
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'src/admin/inc/class-portfolio-admin-metaboxes.php';
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'src/admin/inc/class-portfolio-helpers.php';
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'src/frontend/inc/class-portfolio-frontend-scripts.php';
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'src/frontend/inc/class-portfolio-frontend-page.php';


	}

	private function define_admin_hooks() {

		$plugin_admin = new Plugin_Load_Scripts( PLUGIN_SLUG, PLUGIN_VERSION );

		add_action( 'admin_enqueue_scripts', array($plugin_admin, 'enqueue_styles'), 10, 1 );
		add_action( 'admin_enqueue_scripts', array($plugin_admin, 'enqueue_scripts'), 10, 1 );

	}

	/**
	 * Register all of the hooks related to the public-facing functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_public_hooks() {

		$plugin_public = new Plugin_Load_Frontend_Scripts( PLUGIN_SLUG, PLUGIN_VERSION );

		add_action( 'wp_enqueue_scripts', array($plugin_public, 'enqueue_styles'), 10,1);
		add_action( 'wp_enqueue_scripts', array($plugin_public, 'enqueue_scripts'), 10, 1);
		add_action('wp_ajax_pagination', array($plugin_public, 'get_posts_for_pagination'));
		add_action('wp_ajax_nopriv_pagination', array($plugin_public, 'get_posts_for_pagination'));

	}
    public static function path( $append = '' ) {
		return (string) plugin_dir_path( __FILE__ ) . str_replace( '/', DIRECTORY_SEPARATOR, $append );
	}

	public static function activate() {
		// update_option(POST_TYPE_DETAIL, true);
	}

	public static function deactivate(){
		if(!define(PLUGIN_SLUG, true) && PLUGIN_SLUG==null){
			return;
		}
		// $PostData = get_option(POST_TYPE_DETAIL, true);
		// $data = @unserialize($PostData);
		// unregister_post_type($data['slug']);
		// delete_option(POST_TYPE_DETAIL);
		// flush_rewrite_rules(true);
	}
	public static function delete(){
		if(!define(PLUGIN_SLUG, true) && PLUGIN_SLUG==null){
			return;
		}
		$PostData = get_option(POST_TYPE_DETAIL, true);
		$data = @unserialize($PostData);
		unregister_post_type($data['slug']);
		delete_option(POST_TYPE_DETAIL);
		flush_rewrite_rules(true);
	}
}
