<?php

/**
 * Fired during plugin deactivation
 *
 * @link       https://wordpress.org/
 * @since      1.0.0
 *
 * @package    Web_Portfolio
 * @subpackage Web_Portfolio/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Web_Portfolio
 * @subpackage Web_Portfolio/includes
 * @author     WordPress Dev <admin@wordpress.org>
 */
class Web_Portfolio_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
