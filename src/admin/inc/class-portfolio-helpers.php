<?php

/**
 * Provide a public-facing view for the plugin
 *
 * This file is used to markup the public-facing aspects of the plugin.
 *
 * @link       https://wordpress.org/
 * @since      1.0.0
 *
 * @package    Web_Portfolio
 * @subpackage Web_Portfolio/public/partials
 */


class Plugin_Helpers {

	//Check if slug exist or not
	public static function is_slug_exists($post_name){
		global $wpdb;
		if($wpdb->get_row("SELECT post_name FROM wp_posts WHERE post_name = '" . $post_name . "'", 'ARRAY_A')) {
			return true;
		} else {
			return false;
		}
	}

	//All portfolio details
	public static function portfolio_meta_fields(){
		$ArrPostFields = array(
			array('meta_title' => 'Company Name', 'meta_name' => '_web_portfolio_company'),
			array('meta_title' => 'Website URL', 'meta_name' => '_web_portfolio_website'),
			array('meta_title' => 'Client', 'meta_name' => '_web_portfolio_client'),
		);
		return $ArrPostFields;
	}
	public static function formated_print($array){
		echo '<pre>';
		print_r($array);
		echo '</pre>';
	}
}
new Plugin_Helpers();
