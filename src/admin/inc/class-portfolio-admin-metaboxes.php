<?php
class Plugin_Meta_Boxes
{
    private $slug;
    private $title;
    private $error;
    public function __construct()
    {

        $PostData = get_option(POST_TYPE_DETAIL, true);
		$this->PostData = $PostData;
        if (@unserialize($PostData)) {
            $data = @unserialize($PostData);
            $this->slug = (string)$data['slug'];
            $this->title = (string)$data['title'];
            add_action('add_meta_boxes', [$this, 'create_metaboxes']);
            add_action('save_post', [$this, 'save_metaboxes'], 1, 2);
            if (isset($data['error'])) {
                $this->error = $data['error'];
                add_action('admin_notices', [$this, 'sample_admin_notice__error']);
            }
        }
        $this->reset_notices();
    }

    public function create_metaboxes()
    {
        add_meta_box(
            $this->slug . '_box_id',                 // Unique ID
            $this->title . ' Fields',      // Box title
            array($this, 'display_metaboxes'),  // Content callback, must be of type callable
            $this->slug,                            // Post type
            'normal',
            'high',
        );
    }
    public function display_metaboxes()
    {
        $fields = null;
        global $post;
        $nounce = wp_create_nonce(plugin_basename(__FILE__));
        $key = get_post_meta($post->ID, $this->slug . '_cpt_key', true);
        $value = get_post_meta($post->ID, $this->slug . '_cpt_value', true);
        if(count($value) > 0){
            $fields = $value;
            // $fields = array_combine($key, $value);
        }
        $slug = $this->slug;
        include_once plugin_dir_path(dirname(__FILE__)) . 'template-part/metafield-form.php';
    }
    public function save_metaboxes($post_id, $post)
    {

        $nonce_name   = isset($_POST['_wp_nounce']) ? $_POST['_wp_nounce'] : '';
        $nonce_action = plugin_basename(__FILE__);
        // Check if nonce is valid.
        if (!wp_verify_nonce($nonce_name, $nonce_action)) {
            return;
        }

        // Check if user has permissions to save data.
        if (!current_user_can('edit_post', $post_id)) {
            return;
        }

        // Check if not an autosave.
        if (wp_is_post_autosave($post_id)) {
            return;
        }

        // Check if not a revision.
        if (wp_is_post_revision($post_id)) {
            return;
        }
        /* 
        *----- TODO  -----
        * Check If two keys are not same
        * Make the key as required value.
        */
        if (array_key_exists($this->slug . '_cpt_key', $_POST) && array_key_exists($this->slug . '_cpt_value', $_POST)) {
            $keys = $_POST[$this->slug . '_cpt_key'];
            if (count($keys) !== count(array_unique($keys))) {
                $data = @unserialize(get_option(POST_TYPE_DETAIL, true));
                $data['error'] = 'Cannot have similar key';
                $data = @serialize($data);
                update_option(POST_TYPE_DETAIL, $data);
                return;
            }
            $data =[];
            foreach($_POST[$this->slug . '_cpt_value'] as $key=> $value){
                $data[$key]= htmlspecialchars($value);
            }
            $data = array_combine($keys, $data);
            // Plugin_Helpers::formated_print($data);
            // die();
            $val = update_post_meta(
                $post_id,
                $this->slug . '_cpt_value',
                $data
            );
        }
    }
    function sample_admin_notice__error()
    {
        $class = 'notice notice-error';
        $message = __($this->error, 'Error while saving');
        printf('<div class="%1$s"><p>%2$s</p></div>', esc_attr($class), esc_html($message));
    }
    private function reset_notices()
    {
        $data = @unserialize(get_option(POST_TYPE_DETAIL, true));
        unset($data['error']);
        $data = @serialize($data);
        update_option(POST_TYPE_DETAIL, $data);
    }

}
new Plugin_Meta_Boxes();
