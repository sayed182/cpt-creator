<?php

class Plugin_Admin_pages
{

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct()
	{
		define('POST_TYPE_DETAIL', 'custom_post_creator_details');
		define('FLUSH_LINKS', 'custom_post_creator_links');
		$PostData = get_option(POST_TYPE_DETAIL, true);
		$data = @unserialize($PostData);

		add_action('admin_menu', array($this, 'portfolio_add_naming_page'));
		add_action('admin_menu', array($this, 'web_portfolio_hide_menu_page'));
		add_action('activated_plugin', array($this, 'web_portfolio_redirect'));
		add_action('init', array($this, 'web_portfolio_create_post_type'));
		if ($data !== false) {
			add_action('init', array($this, 'web_portfolio_naming_post_type'));
			add_action('init', array($this, 'register_my_custom_categories'));
			add_action('init', array($this, 'register_my_custom_tags'));
		}
	}

	/**
	 * This function is provided for adding sub menu
	 */
	function portfolio_add_naming_page()
	{
		add_menu_page(
			__('Portfolio Title', 'web-portfolio'),
			'Add Portfolio Title',
			'manage_options',
			PLUGIN_SLUG.'_menu',
			array($this, 'web_portfolio_menu_callback'),
			'dashicons-ellipsis',
			6
		);
	}

	/**
	 * This function is callback of sub menu
	 */
	function web_portfolio_menu_callback()
	{

		//Check if post type has already data
		$post_name = get_option(POST_TYPE_DETAIL, true);
		$data = @unserialize($post_name);
		if ($data !== false) {
			if (headers_sent()) {
				echo "<script>location.replace('" . admin_url('edit.php?post_type=' . $data['slug']) . "'); </script>";
			} else {
				exit(wp_redirect(admin_url('edit.php?post_type=' . $data['slug'])));
			}
		}
		$title = '';
		$slug = '';
		$icon = '';
		if (isset($_POST['save_custom_post_details'])) {
			$title = $_POST['posttype_title'];
			$slug = $_POST['posttype_slug'];
			$icon = $_POST['posttype_icon'];
		}

		//form starts
		$html = '<div class="wrap">';
		$html .= '<form class="bg-white web-portfolio-frm" method="post">';
		$html .= '<h1>' . __('Post Type Details', 'web-portfolio') . '</h1>';

		//portfolio title
		$html .= '<div class="form-group">';
		$html .= '<label>' . __('Portfolio Post Type Title: ', 'web-portfolio') . '</label>';
		$html .= '<input type="text" class="txt-input" placeholder="' . __('Awesome Portfolio', 'web-portfolio') . '" name="posttype_title" value="' . $title . '" required/>';
		$html .= '</div>';

		//portfolio slug
		$html .= '<div class="form-group">';
		$html .= '<label>' . __('Portfolio Post Type Slug: ', 'web-portfolio') . '</label>';
		$html .= '<input type="text" class="txt-input prv-space" placeholder="' . __('awesome-portfolio', 'web-portfolio') . '" name="posttype_slug" value="' . $slug . '" required />';
		$html .= '<small>' . __('Only lowercase words with no space') . '</small>';
		$html .= '</div>';

		//Portfolio Dashicone
		$html .= '<div class="form-group">';
		$html .= '<label>' . __('Portfolio Post Type Icon: ', 'web-portfolio') . '</label>';
		$html .= '<input type="text" class="txt-input prv-space" placeholder="' . __('dashicons-image-filter', 'web-portfolio') . '" name="posttype_icon" value="' . $icon . '" required />';
		$html .= '<small>' . __('Dashicons will be here, here is the URL: <a http"https://developer.wordpress.org/resource/dashicons">https://developer.wordpress.org/resource/dashicons</a>') . '</small>';
		$html .= '</div>';

		//Portfolio details save
		$html .= '<div class="form-group">';
		$html .= '<input type="submit" class="button submit-input" name="save_custom_post_details" value="' . __('Save Details', 'web-portfolio') . '" />';
		$html .= '</div>';

		$html .= '</form>';
		$html .= '</div>';
		echo $html;
	}

	/**
	 * This function will hide sub menu
	 */
	function web_portfolio_hide_menu_page()
	{
		//Hidden the post type setting page
		remove_menu_page(PLUGIN_SLUG.'_menu');
	}

	/**
	 * This function will redirect to portfolio setting page
	 * while plugin will active
	 */
	function web_portfolio_redirect()
	{
		$post_name = get_option(POST_TYPE_DETAIL, true);
		$data = @unserialize($post_name);
		if ($data !== true) {
			if (headers_sent()) {
				echo "<script>location.replace('" . admin_url('admin.php?page='.PLUGIN_SLUG.'_menu') . "'); </script>";
			} else {
				exit(wp_redirect(admin_url('admin.php?page='.PLUGIN_SLUG.'_menu')));
			}
		}
	}

	/**
	 * This function will save all the post type details
	 * and create a custom post as per requirement
	 */
	function web_portfolio_create_post_type()
	{
		if (isset($_POST['save_custom_post_details'])) {

			//Getting details
			$title = $_POST['posttype_title'];
			$slug = $_POST['posttype_slug'];
			$icon = $_POST['posttype_icon'];

			//Slug validation
			$slug_exist = Plugin_Helpers::is_slug_exists($slug);
			if ($slug_exist) {
				echo '<div class="notice notice-error is-dismissible">' . __('This slug is already in use, please try diffrent slug', 'web-portfolio') . '</div>';
			} else if (post_type_exists($slug)) {
				echo '<div class="notice notice-error is-dismissible">' . __('This post type is already in use, please try diffrent name', 'web-portfolio') . '</div>';
			} else {
				echo '<div class="notice notice-success is-dismissible">' . __('Post Type created sucessfully', 'post_type_creator') . '</div>';
				$getOpt = array('title' => $title, 'slug' => $slug, 'icon' => $icon);
				$UpdateOpt = serialize($getOpt);
				update_option(POST_TYPE_DETAIL, $UpdateOpt);
				update_option(FLUSH_LINKS, 'No');
				// update_option('web_portfolio_details', '');
			}
		}
	}


	function web_portfolio_naming_post_type()
	{
		$PostData = get_option(POST_TYPE_DETAIL, true);
		$flush_links = get_option(FLUSH_LINKS, true);
		$PostData = unserialize($PostData);
		$title = $PostData['title'];
		$slug = $PostData['slug'];
		$icon = $PostData['icon'];

		//post lables
		$labels = array(
			'name' => __($title, 'web-portfolio'),
			'singular_name' => __($title, 'web-portfolio'),
			'menu_name' => __($title, 'web-portfolio'),
			'name_admin_bar' => __($title, 'web-portfolio'),
			'add_new' => __('Add New', 'web-portfolio'),
			'add_new_item' => __('Add New ' . $title . '', 'web-portfolio'),
			'new_item' => __('New ' . $title . '', 'web-portfolio'),
			'edit_item' => __('Edit ' . $title . '', 'web-portfolio'),
			'view_item' => __('View ' . $title . '', 'web-portfolio'),
			'all_items' => __('All ' . $title . 's', 'web-portfolio'),
			'search_items' => __('Search ' . $title . '', 'web-portfolio'),
			'parent_item_colon' => __('Parent ' . $title . ':', 'web-portfolio'),
			'not_found' => __('No ' . $title . ' found.', 'web-portfolio'),
			'not_found_in_trash' => __('No ' . $title . ' found in Trash.', 'web-portfolio')
		);

		//Arguments
		$args = array(
			'labels'            => $labels,
			'description'       => $title . ' Listing',
			'public'            => true,
			'menu_position'     => 5,
			'supports'          => array('title', 'editor', 'thumbnail', 'comments',),
			'has_archive'       => true,
			'show_in_admin_bar' => true,
			'show_in_nav_menus' => true,
			'query_var'         => true,
			'menu_icon' 		=> $icon,
			'rewrite'           => array('slug' =>  $slug),
		);
		register_post_type($slug, $args);

		//check for flushing permalinks
		if (!empty($flush_links)) {
			flush_rewrite_rules();
			update_option(FLUSH_LINKS, '');
		}
	}

	/**
	 * Taxonomy: Portfolio Categories.
	 */

	function register_my_custom_categories()
	{
		$PostData = get_option(POST_TYPE_DETAIL, true);
		$flush_links = get_option(FLUSH_LINKS, true);
		$PostData = unserialize($PostData);
		$title = $PostData['title'];
		$slug = $PostData['slug'];
		$icon = $PostData['icon'];
		$labels = [
			"name" => __("Categories", "highstarter"),
			"singular_name" => __("Category", "highstarter"),
			"menu_name" => __("Category", "highstarter"),
			"all_items" => __("All Category", "highstarter"),
			"edit_item" => __("Edit Category", "highstarter"),
			"view_item" => __("View Category", "highstarter"),
			"update_item" => __("Update Category", "highstarter"),
			"add_new_item" => __("Add new category", "highstarter"),
			"new_item_name" => __("New category name", "highstarter"),
			"parent_item" => __("Parent Category", "highstarter"),
			"parent_item_colon" => __(":", "highstarter"),
			"search_items" => __("Search categories", "highstarter"),
			"popular_items" => __("Popular categories", "highstarter"),
			"separate_items_with_commas" => __("Separate items with commas", "highstarter"),
			"add_or_remove_items" => __("add_or_remove_items", "highstarter"),
			"choose_from_most_used" => __("Choose from most used categories", "highstarter"),
			"not_found" => __("No categories found", "highstarter"),
			"no_terms" => __("No categories inserted", "highstarter"),
			"items_list_navigation" => __("Aut ad et placeat c", "highstarter"),
			"items_list" => __("Categories list", "highstarter"),
			"back_to_items" => __("Back ", "highstarter"),
		];

		$args = [
			"label" => __("Categories", "highstarter"),
			"labels" => $labels,
			"public" => true,
			"publicly_queryable" => true,
			"hierarchical" => false,
			"show_ui" => true,
			"show_in_menu" => true,
			"show_in_nav_menus" => true,
			"query_var" => true,
			"rewrite" => ['slug' => $slug . '_category', 'with_front' => true,],
			"show_admin_column" => true,
			"show_in_rest" => true,
			"rest_base" => $slug . "_category",
			"rest_controller_class" => "WP_REST_Terms_Controller",
			"show_in_quick_edit" => false,
			"show_in_graphql" => false,
		];
		register_taxonomy($slug . "_category", [$slug], $args);
	}
	/**
	 * Taxonomy: Portfolio Tags.
	 */
	function register_my_custom_tags()
	{
		$PostData = get_option(POST_TYPE_DETAIL, true);
		$flush_links = get_option(FLUSH_LINKS, true);
		$PostData = unserialize($PostData);
		$title = $PostData['title'];
		$slug = $PostData['slug'];
		$icon = $PostData['icon'];
		$labels = [
			"name" => __("Tags", "highstarter"),
			"singular_name" => __("Tag", "highstarter"),
			"menu_name" => __("Tag", "highstarter"),
			"all_items" => __("All Tag", "highstarter"),
			"edit_item" => __("Edit Tag", "highstarter"),
			"view_item" => __("View Tag", "highstarter"),
			"update_item" => __("Update Tag", "highstarter"),
			"add_new_item" => __("Add new tag", "highstarter"),
			"new_item_name" => __("New tag name", "highstarter"),
			"parent_item" => __("Parent tag", "highstarter"),
			"parent_item_colon" => __(":", "highstarter"),
			"search_items" => __("Search Tags", "highstarter"),
			"popular_items" => __("Popular tags", "highstarter"),
			"separate_items_with_commas" => __("Separate items with commas", "highstarter"),
			"add_or_remove_items" => __("add_or_remove_items", "highstarter"),
			"choose_from_most_used" => __("Choose from most used Tags", "highstarter"),
			"not_found" => __("No Tags found", "highstarter"),
			"no_terms" => __("No Tags inserted", "highstarter"),
			"items_list_navigation" => __("Aut ad et placeat c", "highstarter"),
			"items_list" => __("Tags list", "highstarter"),
			"back_to_items" => __("Back ", "highstarter"),
		];
		$args = [
			"label" => __("Tags", "highstarter"),
			"labels" => $labels,
			"public" => true,
			"publicly_queryable" => true,
			"hierarchical" => false,
			"show_ui" => true,
			"show_in_menu" => true,
			"show_in_nav_menus" => true,
			"query_var" => true,
			"rewrite" => ['slug' => $slug.'_tag', 'with_front' => true,],
			"show_admin_column" => true,
			"show_in_rest" => true,
			"rest_base" => $slug."_tag",
			"rest_controller_class" => "WP_REST_Terms_Controller",
			"show_in_quick_edit" => false,
			"show_in_graphql" => false,
		];
		register_taxonomy($slug."_tag", [$slug], $args);
	}
}
new Plugin_Admin_pages();
