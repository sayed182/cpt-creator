<input type="hidden" name="post_type_name" value="<?php echo $slug;?>">
<input type="hidden" name="_wp_nounce" value="<?php echo isset($nounce)?$nounce:''; ?>">
<table class="post-type-creator-cpt-table">
    <thead>
        <tr>
            <td>
                Field Name
            </td>
            <td>
                Field Value
            </td>
        </tr>
    </thead>
    <tbody id="_wp_portfolio_meta_table">
        <?php 
        $index = 0;
        if(null !== $fields ){
            foreach($fields as $key => $value){
        ?>
        <tr id="row-<?php echo $index;?>">
            <td class="post-type-creator__key">
                <input name='<?php echo $slug;?>_cpt_key[]' class="newtag " value="<?php echo $key;?>" />
            </td>
            <td class="post-type-creator__value">
                <input name='<?php echo $slug;?>_cpt_value[]' value="<?php echo htmlspecialchars_decode($value);?>" />
            </td>
            <td style="max-width:30px;">
                <button type="button" class="post-type-creator-remove-new" data-delete-index="<?php echo $index++; ?>">X</button>
            </td>
        </tr>
        <?php }}else{?>
        <tr>
            <td class="post-type-creator__key">
                <input name='<?php echo $slug;?>_cpt_key[]' class="newtag form-input-tip ui-autocomplete-input" />
            </td>
            <td class="post-type-creator__value">
                <input name='<?php echo $slug;?>_cpt_value[]' />
            </td>
        </tr>
        <?php } ?>
    </tbody>
</table>
<button id="post-type-creator-add-new" type="button">
    Add New
</button>
