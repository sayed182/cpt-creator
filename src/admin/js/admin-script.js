(function ($) {
    var slug = $('input[name="post_type_name"]').val();
    var fields = get_metafields_template(slug);
    var count = 1;
        $('#post-type-creator-add-new').on('click', function (event) {
            event.preventDefault();
            if (!fields) {
                alert('Something Went Wrong. Please reactivate the plugin');
                return;
            }
            $('#_wp_portfolio_meta_table').append(`<tr id="row-${count}">${get_metafields_template(slug, count++)}</tr>`)
        });
        
        $('body').on('click', 'button.post-type-creator-remove-new',function (event) {
            event.preventDefault();
            var index= $(event.target).data('delete-index');
            $("#_wp_portfolio_meta_table tr#row-"+index).remove();
        });

        function get_metafields_template(slug, index) {

            return `<td class="post-type-creator__key">
                        <input name='${slug}_cpt_key[]' class="newtag form-input-tip ui-autocomplete-input" />
                    </td>
                    <td class="post-type-creator__value">
                        <input name='${slug}_cpt_value[]' />
                    </td>
                    <td style="max-width:30px;">
                    <button type="button" class="post-type-creator-remove-new" data-delete-index="${index}">X</button>
                    </td>
                    `;
        }

})(jQuery);



