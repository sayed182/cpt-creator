<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              https://wordpress.org/
 * @since             1.0.0
 * @package           Web_Portfolio
 *
 * @wordpress-plugin
 * Plugin Name:       Post Type Creator
 * Plugin URI:        https://wordpress.org/
 * Description:       This is a short description of what the plugin does. It's displayed in the WordPress admin area.
 * Version:           1.0.0
 * Author:            WordPress Dev
 * Author URI:        https://wordpress.org/
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Currently plugin version.
 * Start at version 1.0.0 and use SemVer - https://semver.org
 * Rename this for your plugin and update it as you release new versions.
 */
define( 'PLUGIN_VERSION', '1.0.0' );
define( 'PLUGIN_NAME', 'Post Type Creator' );
define( 'PLUGIN_SLUG', 'post_type_creator' );

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-web-portfolio-activator.php
 */
function activate_web_portfolio() {
	Plugin::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-web-portfolio-deactivator.php
 */
function deactivate_web_portfolio() {
	Plugin::deactivate();
}

function uninstall_web_portfolio( ){
	Plugin::delete();
}

register_activation_hook( __FILE__, 'activate_web_portfolio' );
register_deactivation_hook( __FILE__, 'deactivate_web_portfolio' );
register_uninstall_hook( __FILE__, 'uninstall_web_portfolio' );

require_once 'src/class-plugin-main.php';

function initialize_plugin(){

	$plugin = new Plugin();
}

initialize_plugin();